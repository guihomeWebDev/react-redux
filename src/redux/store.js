import {createStore, combineReducers, applyMiddleware} from 'redux';
import CounterReducer from './/Reducer/CounterReducer';
import AddCartReducer from './Reducer/AddCartReducer';
import dataReducer from './Reducer/dataReducer';
import thunk from 'redux-thunk';

const rootReducer = combineReducers({
     CounterReducer,
     AddCartReducer,
     dataReducer
})

//creation d'un middleware sans thunk on passe donc customMiddleware à applyMiddleware()
// const customMiddleware = store => next => action => {
//     console.log('Middleware', action);
//     next(action);
// }

const store = createStore(rootReducer, applyMiddleware(thunk));

export default store;