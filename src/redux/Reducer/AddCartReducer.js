const INITIAL_STATE = {
    cart: 50,
    // isLoading: false,
    // error: null
};

function AddCartReducer(state = INITIAL_STATE, action) {
    
    switch(action.type) {
        case 'ADDCART': {
            return {
                ...state,
                cart: action.payload
            }
        }
    }

    return state;
}

export default AddCartReducer;