const INITIAL_STATE = {
    imageURL: "",
    // isLoading: false,
    // error: null
};

function DataImgReducer(state = INITIAL_STATE, action) {
    
    switch(action.type) {
        case 'LOADING': {
            return {
                ...state,
                imgURL: action.payload
            }
        }
    }

    return state;
}

export default DataImgReducer;

export const getCatImg = () => dispatch => {
    fetch("https://api.thecatapi.com/v1/images/search")
    .then(res => res.json())
    .then(data => {
        dispatch({
            type: 'LOADING',
            payload: data[0].url
        })
    })
}