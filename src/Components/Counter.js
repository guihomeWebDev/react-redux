import React, {useState, useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getCatImg } from '../redux/Reducer/dataReducer';

export default function Counter() {

    const[cartData, setCartData] = useState(0);

    const{cart, count, imgURL} = useSelector(state => ({
        ...state.AddCartReducer,
        ...state.CounterReducer,
        ...state.dataReducer
    }));

    const dispatch = useDispatch()

    const incfunc = () => {
        dispatch({ type: 'INCREMENT' })
    }

    const decFunc = () => {
        dispatch({ type: 'DECREMENT' })
    }

    const addCart = () => {
        dispatch({ 
            type: 'ADDCART', 
            payload: cartData 
        })
    }

    useEffect(() => {
        dispatch(getCatImg())
    },[])


  return (
    <div>
        <h2>Les donnees Panier: {cart}</h2>
        <h2>Les données compteur : {count}</h2>
        <button onClick={incfunc} >+1</button>
        <button onClick={decFunc}>-1</button>
        <input
            value={cartData}
            onInput={e => setCartData(e.target.value)}
            type="number"
          />
        <button onClick={addCart}>Ajouter au panier</button>
        {imgURL &&<img style={{width: '200px'}}src={imgURL} alt="cat" />}

    </div>
  )
}
